import axios from "axios";
import { ILivre } from "./entities";

export async function fetchAllBooks(token = null){
    const response = await axios.get<ILivre[]>('/api/book');
    return response.data;
}


export async function dispo(id: any, token=null) {
    const response = await axios.post<ILivre>(`/api/book/${id}/dispo`, id);
    return response.data;
  }