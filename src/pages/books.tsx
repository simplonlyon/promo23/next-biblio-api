import { fetchAllBooks } from '@/book-services';
import ItemBooks from '@/components/ItemBooks';
import { ILivre, User } from '@/entities';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useContext, useEffect, useState } from 'react';
import {Table} from 'react-bootstrap';
import { AuthContext } from "@/auth/auth-context";
import { fetchUser } from '@/auth/auth-service';
import { useRouter } from "next/router";




export default function BookList() {
  const { token, setToken } = useContext(AuthContext);
   const [user, setUser] = useState<User>();
   const router = useRouter();
   const [book, setBook]= useState<ILivre[]>();

   useEffect(() => {
    const fetchData = async () => {
      try {
        const user = await fetchUser();
        setUser(user);
        const book = await fetchAllBooks();
        setBook(book);
      } catch (error:any) {
        if (error.response.status === 401) {
          router.push('/login');
        }
      }
    };
    fetchData();
  }, [router]);



  return (
    
    <div className='container mt-5'>
      {token && (
  <>
    <h2>Bonjour {user?.email} -  <p>Role : {user?.role}</p></h2> 
     
     <Table striped bordered hover>
      <thead>
        <tr>
          <th>Id</th>
          <th>Titre</th>
          <th>Auteur</th>
          <th>Description</th>
          <th>Dispo</th>
          <th colSpan={3} className="text-center">Actions</th>
        </tr>
      </thead>
      <tbody>
        { book?.map(livre => 
          <ItemBooks key={livre.id} livre={livre} userData={user} />
        )

        }
       
      </tbody>
    </Table>
  </>
)}
    

    </div>
  )
}


