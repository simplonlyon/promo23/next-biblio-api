import { AuthContextProvider } from '@/auth/auth-context';
import Menu from '@/components/Menu';
import '@/styles/globals.css'
import axios from 'axios';
import type { AppProps } from 'next/app';
import '../auth/axios-config';



axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;


export default function App({ Component, pageProps}: AppProps) {

  return( 
    <>
  <Menu />
       
   <AuthContextProvider>
      <Component {...pageProps} />
    </AuthContextProvider>
    </>
    

  )
}

