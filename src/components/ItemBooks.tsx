
import { dispo } from '@/book-services';
import { ILivre, User } from '@/entities';
import Link from 'next/link';
import { useState } from 'react';
import { Button} from 'react-bootstrap';

interface Props{
    livre:ILivre;
    userData?: User;
   
}



export default function ITemLivre({livre, userData}:Props ){
  const [isDispo, setIsDispo] = useState(true);
  
  // fonction qui fait appel au service dispo , pour emprunter donc qui permet de faie un post pour changer la valeur de dispo de 1 à 0
  const emprunter= async () => {
    try {
      // faut lui passer l'id du livre sinon l'app ne sait pas quel livre mettre en non dispo
      const updatedBook = await dispo(livre.id);
      // setIsDispo c'est pour gerer la couleur du bouton quand y'a bien un pos
      setIsDispo(false);
      return updatedBook;
    } catch (error) {
      console.error(error);
    }
  };


  return (
    <>
        <tr>
            <td>{livre.id}</td>
            <td>{livre.name}</td>
            <td>{livre.author}</td>
            <td>{livre.description}</td>
            <td>{livre.dispo && isDispo ? <Button variant="success" onClick={emprunter}>Emprunter</Button>:<Button disabled >Indisponible</Button>} </td>
            <td>{livre.date_emprunt }</td>
          {/*   selectionner un livre et l'afficher par le modal */}
          {userData?.role === 'ROLE_ADMIN' ? (
            <>
              <td><Button variant="info" >Voir</Button></td>
              <td> <Button variant="warning">Modifier</Button></td>
              <td> <Button variant="danger" >Supprimer</Button></td>  
            </>
          ) : (
      <></>
          )}
            
        </tr>
    </>
  )
}
