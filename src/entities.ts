export interface ILivre {
    id?: number;
    name?: string;
    author?: string;
    description?: string;
    dispo?:number;
    date_emprunt?:string
}

export interface User{
    id?:number;
    email:string;
    password:string;
    role?:string;
}

